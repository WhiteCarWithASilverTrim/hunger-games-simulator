/// This acts as a simple server to serve the files that make up the Hunger Games Simulator.
///
/// This server is NOT the server used by nguh.org; pull requests that modify this file will
/// have no immediate effect on the version of the simulator hosted on nguh.org.

const fs = require('fs')
const http = require('http')
const {spawnSync} = require('child_process')

function main() {
    /// Make sure we're in the right directory
    process.chdir(__dirname)

    /// Initialise the repository
    if (process.argv.includes('--init')) {
        init()
        process.exit(0)
    }

    /// Start the server
    const server = http.createServer(async (req, res) => {
        /// Try to send the file that the client requested.
        try {
            /// We don't handle anything other than GET or HEAD.
            if (req.method !== 'GET' && req.method !== 'HEAD') return err(400, res)

            /// Decode the URI.
            const uri = decodeURI(req.url)
            const path = `${__dirname}/${uri}`

            /// If this path doesn't exist, then delegate the request to
            /// the actual nguh.org server so long as it's just an image.
            if (!fs.existsSync(path)) {
                if (content_type(path).startsWith('image')) {
                    res.statusCode = 302
                    res.setHeader('Location', `https://www.nguh.org/${uri}`)
                    return res.end()
                }

                return err(404, res)
            }

            /// Get the file content.
            const file = fs.readFileSync(path)

            /// Send it.
            res.statusCode = 200
            res.setHeader('Content-Type', content_type(path))
            if (req.method === 'HEAD') res.end()
            else res.end(file)
        }

            /// If anything goes wrong, respond with a 500.
        catch (e) {
            console.error(e)
            err(500, res)
        }
    })

    const port = 24005
    server.listen(port, 'localhost', () => {
        console.log(`Server is listening on http://localhost:${port}/apps/hgs/hunger_games_simulator.html`)
    })

    /// Signal an error.
    function err(code, res) {
        res.statusCode = code
        res.end()
    }

    /// Determine the MIME type of a file based on its extension.
    function content_type(path) {
        const extension = path.slice(path.lastIndexOf('.'))
        switch (extension) {
            case '.html':
                return 'text/html; charset=utf-8'
            case '.js':
                return 'text/javascript; charset=utf-8'
            case '.css':
                return 'text/css; charset=utf-8'
            case '.woff':
                return 'font/woff; charset=utf-8'
            case '.wasm':
                return 'application/wasm'
            case '.pdf':
                return 'application/pdf'
            case '.svg':
                return 'image/svg+xml'
            case '.ico':
                return 'image/ico'
            case '.gif':
                return 'image/gif'
            case '.png':
                return 'image/png'
            case '.jpg':
            case '.jpeg':
                return 'image/jpeg'
        }
        return 'text'
    }
}

/// This function does exactly what you think it does.
function die(message) {
    process.stderr.write(`\e[1;31mError: \e[31m${message}\n\e[m`)
    process.exit(1)
}

function init() {
    /// If 'common' already exists, then we're done.
    if (fs.existsSync('./common/.gitkeep')) return

    /// 'common' must contain '.gitkeep'
    if (fs.existsSync('./common'))
        die(`Expected directory '${__dirname}/common' to contain the nguh.org frontend library.\n`
            + "Automatic download is impossible, since the directory exists, but its contents are incorrect.\n" +
            "Please delete it and try running this script again, or add the library manually.")

    /// If 'common' doesn't exist, we need to create it. If this is the main
    /// nguh.org repo, then there should be a 'common' directory in its root
    /// that we can link to.
    const path = `${__dirname}/../../common`
    if (fs.existsSync(`${path}/.gitkeep`)) {
        fs.symlinkSync(path, 'common', 'dir')
        return
    }

    /// If this is not the main nguh.org repo, then we can try cloning the
    /// frontend code from GitLab. We do that with a timeout to make sure
    /// this script doesn't hang indefinitely
    const clone_command = 'git clone --recursive https://gitlab.com/agma-schwa-public/nguh.org-frontend common'
    const timeout = setInterval(() => void die(`Child process '${clone_command}' timed out`), 30000)

    /// Clone the repo.
    const child = spawnSync("git", clone_command.split(' '), {
        cwd: __dirname,
        env: process.env,
        stdio: 'ignore'
    })
    clearTimeout(timeout)

    /// If that didn't work either, give up.
    if (!fs.existsSync('./common/.gitkeep')) die("Could not initialise 'common'")
}

main()